
package testeacademia;

import java.sql.Time;
import java.util.Date;

public class Agenda 
{
    private Aluno horarioAluno;
    private String treino;
    private Date horario;

    public Agenda(Aluno horarioAluno, String treino, Date horario) 
    {
        this.horarioAluno = horarioAluno;
        this.treino = treino;
        this.horario = horario;
    }
}
