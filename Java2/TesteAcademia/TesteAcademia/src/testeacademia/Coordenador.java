
package testeacademia;
public class Coordenador extends Funcionario
{
    private SupervisorTecnico chefe;

    public Coordenador(String nome, double salario, String endereco, SupervisorTecnico chefe) 
    {
        super(nome, salario, endereco);
        this.chefe = chefe;
    }

    public SupervisorTecnico getChefe() {
        return chefe;
    }
}
