
package testeacademia;

public class Funcionario 
{
    protected String nome;
    protected double salario;
    protected String endereco;
    
    public Funcionario(String nome, double salario, String endereco)
    {
        this.nome = nome;
        this.salario = salario;
        this.endereco = endereco;
    }
}
