
package testeacademia;
public class Gerente extends Funcionario
{
    private DiretorGeral chefe;
    
    public Gerente(String nome, double salario, String endereco, DiretorGeral chefe) 
    {
        super(nome, salario, endereco);
        this.chefe = chefe;
    }
    
    public DiretorGeral getChefe()
    {
        return this.chefe;
    }
}
