
package testeacademia;

class Professor 
{
    protected int numeroAlunosAtendidos;
    protected String treinoMinistrado;
    protected Agenda agenda;
    protected Coordenador chefe;

    public Professor(int numeroAlunosAtendidos, String treinoMinistrado, Agenda agenda, Coordenador chefe) 
    {
        this.numeroAlunosAtendidos = numeroAlunosAtendidos;
        this.treinoMinistrado = treinoMinistrado;
        this.agenda = agenda;
        this.chefe = chefe;
    }  

    public Coordenador getChefe() {
        return chefe;
    }
}
