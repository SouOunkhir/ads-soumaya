
package testeacademia;

public class Recepcionista extends Funcionario
{
    private SupervisorRecepcaoVendas chefe;
    
    public Recepcionista(String nome, double salario, String endereco, SupervisorRecepcaoVendas chefe) 
    {
        super(nome, salario, endereco);
        this.chefe = chefe;
    }
    
    public void agendar()
    {
        
    }

    public SupervisorRecepcaoVendas getChefe() {
        return chefe;
    }
}
