
package testeacademia;

public class ServicosGerais extends Funcionario
{
    
    private SupervisorLimpezaManutencao chefe;
    
    public ServicosGerais(String nome, double salario, String endereco, SupervisorLimpezaManutencao chefe) 
    {
        super(nome, salario, endereco);
        this.chefe = chefe;
    }
    
    public void limpar()
    {
        System.out.println("Limpeza Finalizada!");
        return;
    }

    public SupervisorLimpezaManutencao getChefe() {
        return chefe;
    }
}
