
package testeacademia;

public class SupervisorLimpezaManutencao extends Funcionario
{

    public SupervisorLimpezaManutencao(String nome, double salario, String endereco) 
    {
        super(nome, salario, endereco);
    }
    
    
    public void gerarRelatorio()
    {
        System.out.println("Relatório gerado!");
        return;
    }
}
