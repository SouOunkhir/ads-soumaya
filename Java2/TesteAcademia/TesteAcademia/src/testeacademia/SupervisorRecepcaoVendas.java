
package testeacademia;

public class SupervisorRecepcaoVendas extends Funcionario
{ 
    public SupervisorRecepcaoVendas(String nome, double salario, String endereco) 
    {
        super(nome, salario, endereco);
    }
    
    public void gerarRelatorio()
    {
        System.out.println("Relatório gerado!");
        return;
    }
}
