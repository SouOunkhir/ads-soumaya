
package testeacademia;
public class SupervisorTecnico extends Funcionario
{
    private Gerente chefe;
    
    public SupervisorTecnico(String nome, double salario, String endereco, Gerente chefe) 
    {
        super(nome, salario, endereco);
        this.chefe = chefe;
    }

    public Gerente getChefe() {
        return chefe;
    }
    
}
