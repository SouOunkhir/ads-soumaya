package testeacademia;

public class TesteAcademia 
{
    public static void main(String[] args) 
    {
        DiretorGeral diretor1 = new DiretorGeral("Alfredo da Silva", 25000, "Rua Delamare 1425");
        Gerente gerente1 = new Gerente("Laura do Mato", 5000, "Rua Casa Branca", diretor1);
        
        System.out.println("Dados do Diretor Geral da Empresa: ");
        System.out.println(diretor1.nome+"\n"+diretor1.endereco+"\n"+diretor1.salario);
        
        System.out.println();
        
        diretor1 = gerente1.getChefe();
        
        System.out.println("Dados do gerente da empresa: ");
        System.out.println("Nome: "+gerente1.nome+"\n"+"Endereco: "
                            +gerente1.endereco+"\n"+"Salario: "+gerente1.salario
                            +"\n"+"Nome do seu chefe: "+diretor1.getNome());
        
        
        
    }  
}
