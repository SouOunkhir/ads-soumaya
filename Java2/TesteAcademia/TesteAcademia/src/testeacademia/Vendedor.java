
package testeacademia;

public class Vendedor extends Funcionario
{
    private SupervisorRecepcaoVendas chefe;
    
    public Vendedor(String nome, double salario, String endereco, SupervisorRecepcaoVendas chefe) 
    {
        super(nome, salario, endereco);
        this.chefe = chefe;
    }
    
    public void elaborarPacote()
    {
        System.out.println("Pacote elaborado!");
        return;
    }

    public SupervisorRecepcaoVendas getChefe() {
        return chefe;
    }
}
