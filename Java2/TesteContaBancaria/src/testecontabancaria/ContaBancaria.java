
package testecontabancaria;

public class ContaBancaria 
{
    protected String titular;
    protected int numeroAgencia;
    protected int numeroConta;
    protected double saldo;

    public String getTitular() 
    {
        return titular;
    }

    public int getNumeroAgencia() 
    {
        return numeroAgencia;
    }

    public int getNumeroConta() 
    {
        return numeroConta;
    }

    public double getSaldo() 
    {
        return saldo;
    }

    public void setTitular(String titular) 
    {
        this.titular = titular;
        return;
    }

    public void setNumeroAgencia(int numeroAgencia) 
    {
        this.numeroAgencia = numeroAgencia;
        return;
    }

    public void setNumeroConta(int numeroConta) 
    {
        this.numeroConta = numeroConta;
        return;
    }

    public void setSaldo(double saldo) 
    {
        this.saldo = saldo;
        return;
    }
    
    
    public double verificarSaldo()
    {
        return this.saldo;
    }
    
    public void depositar(double deposito)
    {
        saldo = saldo +deposito;
        return;
    }
    
    public void sacar(double valor)
    {
        if (saldo < valor)
        {
            System.out.println("Saldo insuficiente");
        }
        else
        {
            saldo = saldo - valor;
            System.out.println("Saque realizado com sucesso!");
        }
        
        return;
    }
      
            
}
