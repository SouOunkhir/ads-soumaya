
package testecontabancaria;

public class ContaCorrente extends ContaBancaria
{
    public ContaCorrente (String titular, int numeroAgencia, int numeroConta, double saldo)
    {
        if (saldo <50)
        {
            System.out.println("Impossível criar conta corrente. Saldo insuficiente.");
        }
        else
        {
            this.titular = titular;
            this.numeroConta = numeroConta;
            this.numeroAgencia = numeroAgencia;
            this.saldo = saldo;
            
            System.out.println("Conta criada com sucesso!");
        }
    }
    
    
    public void aplicarServico()
    {
        this.saldo = this.saldo - 38;
    }
}
