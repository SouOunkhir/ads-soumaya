/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testecontabancaria;

/**
 *
 * @author laboratorioa
 */
public class ContaPoupanca extends ContaBancaria
{
    public ContaPoupanca(String titular, int numeroAgencia, int numeroConta, double saldo)
    {
        if (saldo < 100)
        {
            System.out.println("Impossível criar conta poupanca. Saldo insuficiente.");
        }
        else 
        {
            this.titular = titular;
            this.numeroConta = numeroConta;
            this.numeroAgencia = numeroAgencia;
            this.saldo = saldo;
            
            System.out.println("Conta criada com sucesso!");
        }
    }
    
    public void rendimento()
    {
        saldo = saldo * 1.189;
    }
}
