
package testecontacorrente;

public class ContaCorrente 
{
    protected Double valor;
    protected double saldo;
    protected int numConta;
    protected String titular;

    public ContaCorrente(double saldo, int numConta, String titular) 
    {
        this.saldo = saldo;
        this.numConta = numConta;
        this.titular = titular;
    }
    
 
    public void depositar(double valor)
    {
        saldo = saldo + valor;
    }
      
    public void sacar(double valor)
    {
        if(saldo < valor)
        {
            System.out.println("Saldo insuficiente.");
        }
        else
        {
            saldo = saldo - valor - (valor*0.0075);
        }
    }
            
    public double consultarSaldo()
    {
        return this.saldo;
    }
            
    public void imprimirExtrato()
    {
        System.out.print("Numero da conta: " + this.numConta + "\n" 
                            + "Titular: " + this.titular + "\n" 
                            + "Saldo: " + this.saldo);
    }
}
