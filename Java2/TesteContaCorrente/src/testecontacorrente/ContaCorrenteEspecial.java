
package testecontacorrente;

public class ContaCorrenteEspecial extends ContaCorrente
{
    public ContaCorrenteEspecial(double saldo, int numConta, String titular) 
    {
        super(saldo, numConta, titular);
    }
    
    public void sacar(double valor)
    {
        if(saldo < valor)
        {
            System.out.println("Saldo insuficiente.");
        }
        else
        {
            saldo = saldo - valor - (valor*0.00125);
        }
    }
}
