
package testecontacorrente;

public class TesteContaCorrente 
{
    public static void main(String[] args) 
    {
        ContaCorrente cc = new ContaCorrente(100, 13425, "Soumaya");
        ContaCorrenteEspecial cce = new ContaCorrenteEspecial(100, 16725, "Crise");
        
        cc.imprimirExtrato();
        System.out.println("\n");
        cce.imprimirExtrato();
        
        cc.depositar(200);
        System.out.println("\n");
        cce.depositar(200);
        
        System.out.println();
        
        cc.imprimirExtrato();
        System.out.println("\n");
        cce.imprimirExtrato();
        
        System.out.println();
        
        cc.sacar(100);
        System.out.println("\n");
        cce.sacar(100);
        
        System.out.println();
        
        cc.imprimirExtrato();
        System.out.println("\n");
        cce.imprimirExtrato();
        
        System.out.println();
    }
}
