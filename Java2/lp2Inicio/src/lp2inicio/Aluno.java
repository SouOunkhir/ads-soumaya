/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp2inicio;

/**
 *
 * @author frank
 */
public class Aluno {
    private String nome;
    private int idade;
    private long cpf;
    private String ra;
    private int semestre;

    public Aluno(String nome, int anos, long cpf, String registroAcad, int semestre){
        this.nome = nome;
        this.idade = anos;
        this.cpf = cpf;
        this.ra = registroAcad;
        this.semestre = semestre;
    }
    
    public Aluno(){
        
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getRa() {
        return ra;
    }

    public void setRa(String ra) {
        this.ra = ra;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }
    
    
}
