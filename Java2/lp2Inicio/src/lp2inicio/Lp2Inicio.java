/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp2inicio;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author frank
 */
public class Lp2Inicio {

    public static void main(String[] args) {
        String opcao;
        List<Aluno> listaAlunos = new ArrayList<>();
        List<Professor> listaProfessores = new ArrayList<>();

        do {
            opcao = JOptionPane.showInputDialog("1 - para salvar Aluno\n"
                    + "2 - para salvar Professor\n3 - para Sair");
            switch (opcao) {
                case "1":
                    Aluno aluno = new Aluno();
                    aluno.setNome(JOptionPane.showInputDialog("Entre com o nome:"));
                    try {
                        aluno.setIdade(Integer.parseInt(JOptionPane.showInputDialog("Entre com a Idade: ")));
                    } catch (NumberFormatException erro) {
                        JOptionPane.showMessageDialog(null, "Caracter inserido incorretamente");
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    aluno.setCpf(Long.parseLong(JOptionPane.showInputDialog("entre com o CPF: ")));
                    aluno.setRa(JOptionPane.showInputDialog("Entre com o RA: "));
                    aluno.setSemestre(Integer.parseInt(JOptionPane.showInputDialog("Entre com o Semestre: ")));
                    listaAlunos.add(aluno);
                    break;
                case "2":
                    Professor prof = new Professor();
                    prof.setNome(JOptionPane.showInputDialog("Entre com o nome:"));
                    try {
                        prof.setIdade(Integer.parseInt(JOptionPane.showInputDialog("Entre com a Idade: ")));
                    } catch (NumberFormatException erro) {
                        JOptionPane.showMessageDialog(null, "Caracter inserido incorretamente");
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    prof.setCpf(Long.parseLong(JOptionPane.showInputDialog("entre com o CPF: ")));
                    prof.setSiape(JOptionPane.showInputDialog("Entre com o SIAPE: "));
                    listaProfessores.add(prof);
                    break;
                case "3":
                    //Sair da aplicação
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção Incorreta",
                            "Sistema Escola", 2);
            }
        } while (!opcao.equals("3"));
    }

}
