/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp2inicio;

/**
 *
 * @author TOSHIBA
 */
public class Professor {
    private String nome;
    private int idade;
    private long cpf;
    private String siape;

    public Professor(String nome, int idade, long cpf, String siape) {
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
        this.siape = siape;
    }
    
    public Professor()
    {}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getSiape() {
        return siape;
    }

    public void setSiape(String siape) {
        this.siape = siape;
    }
    
    
}
