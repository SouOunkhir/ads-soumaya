package br.com.ifms.controller;

import br.com.ifms.model.DisciplinaModel;
import br.com.ifms.db.Banco;
import java.util.LinkedList;

public class DisciplinaController {

    private DisciplinaModel buscado;
    Banco banco = new Banco();
    LinkedList<DisciplinaModel> listaDisciplina = new LinkedList<DisciplinaModel>();

    public void salvar(String nome) {
        DisciplinaModel disciplina = new DisciplinaModel(nome);
        listaDisciplina.add(disciplina);
    }

    public void criar(String nome) {
        DisciplinaModel disciplina = new DisciplinaModel(nome);
        listaDisciplina.add(disciplina);
    }

    public void alterar(DisciplinaModel disciplinaVelha, DisciplinaModel disciplinaNova) {
        buscar(disciplinaVelha);
        listaDisciplina.remove(disciplinaVelha);
        listaDisciplina.add(disciplinaNova);
    }

    public void excluir(DisciplinaModel disciplina) {
        buscar(disciplina);
        listaDisciplina.remove(disciplina);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public DisciplinaModel buscar(DisciplinaModel disciplina) {
        for (int i = 0; i < listaDisciplina.size(); i++) {
            if (disciplina.equals(listaDisciplina.get(i))) {
                buscado = disciplina;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
