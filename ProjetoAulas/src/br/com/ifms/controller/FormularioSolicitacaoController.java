
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.util.LinkedList;


public class FormularioSolicitacaoController {
     private FormularioSolicitacaoModel buscado;
    
    Banco banco = new Banco();
    LinkedList<FormularioSolicitacaoModel> lista = new LinkedList<FormularioSolicitacaoModel>();

    public void salvar(String nome, int matricula, String cargo, String setor,int telefone, String email) {
       FormularioSolicitacaoModel formularioSolicitacao = new FormularioSolicitacaoModel(nome,matricula, cargo, setor, telefone, email);
        lista.add(formularioSolicitacao);
    }

    public void criar(String nome, int matricula, String cargo, String setor,int telefone, String email) {
       FormularioSolicitacaoModel formularioSolicitacao = new FormularioSolicitacaoModel(nome,matricula, cargo, setor, telefone, email);
        lista.add(formularioSolicitacao);
    }

    public void alterar(FormularioSolicitacaoModel velha, FormularioSolicitacaoModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(FormularioSolicitacaoModel formularioSolicitacao) {
        buscar(formularioSolicitacao);
        lista.remove(formularioSolicitacao);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public FormularioSolicitacaoModel buscar(FormularioSolicitacaoModel formularioSolicitacao) {
        for (int i = 0; i < lista.size(); i++) {
            if (formularioSolicitacao.equals(lista.get(i))) {
                buscado = formularioSolicitacao;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
