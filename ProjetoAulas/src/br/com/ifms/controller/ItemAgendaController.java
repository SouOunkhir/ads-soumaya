
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.sql.Time;
import java.util.Date;
import java.util.LinkedList;


public class ItemAgendaController {
    private ItemAgendaModel buscado;
    
    Banco banco = new Banco();
    LinkedList<ItemAgendaModel> lista = new LinkedList<ItemAgendaModel>();

    public void salvar(Time hora, Date data, ProfessorModel professor, TurmaModel turma) {
       ItemAgendaModel itemAgenda = new ItemAgendaModel(hora, data, professor, turma);
        lista.add(itemAgenda);
    }

    public void criar(Time hora, Date data, ProfessorModel professor, TurmaModel turma) {
       ItemAgendaModel itemAgenda = new ItemAgendaModel(hora, data, professor, turma);
        lista.add(itemAgenda);
    }

    public void alterar(ItemAgendaModel velha, ItemAgendaModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(ItemAgendaModel itemAgenda) {
        buscar(itemAgenda);
        lista.remove(itemAgenda);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public ItemAgendaModel buscar(ItemAgendaModel itemAgenda) {
        for (int i = 0; i < lista.size(); i++) {
            if (itemAgenda.equals(lista.get(i))) {
                buscado = itemAgenda;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
