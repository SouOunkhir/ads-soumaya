
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.util.LinkedList;

public class ItemSubstituicaoController {
     private ItemSubstituicaoModel buscado;
    
    Banco banco = new Banco();
    LinkedList<ItemSubstituicaoModel> lista = new LinkedList<ItemSubstituicaoModel>();

    public void salvar(String unidadeCurricular, String curso, String dataSubstituicao, String objetivoAula, String conteudoMinistrado, String atividadesDesenvolvidas, String nomeSubstituto) {
       ItemSubstituicaoModel itemSubstituicao = new ItemSubstituicaoModel(unidadeCurricular, curso, dataSubstituicao, objetivoAula, conteudoMinistrado, atividadesDesenvolvidas, nomeSubstituto);
        lista.add(itemSubstituicao);
    }

    public void criar(String unidadeCurricular, String curso, String dataSubstituicao, String objetivoAula, String conteudoMinistrado, String atividadesDesenvolvidas, String nomeSubstituto) {
       ItemSubstituicaoModel itemSubstituicao = new ItemSubstituicaoModel(unidadeCurricular, curso, dataSubstituicao, objetivoAula, conteudoMinistrado, atividadesDesenvolvidas, nomeSubstituto);
        lista.add(itemSubstituicao);
    }

    public void alterar(ItemSubstituicaoModel velha, ItemSubstituicaoModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(ItemSubstituicaoModel itemSubstituicao) {
        buscar(itemSubstituicao);
        lista.remove(itemSubstituicao);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public ItemSubstituicaoModel buscar(ItemSubstituicaoModel itemSubstituicao) {
        for (int i = 0; i < lista.size(); i++) {
            if (itemSubstituicao.equals(lista.get(i))) {
                buscado = itemSubstituicao;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
