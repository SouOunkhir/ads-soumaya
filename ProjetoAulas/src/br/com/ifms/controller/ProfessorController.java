
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.util.LinkedList;


public class ProfessorController {

    static Banco banco = new Banco();

    public void salvar(String nome, String email) {
        ProfessorModel professor = new ProfessorModel(nome, email);
        banco.preencherListaProfessor(professor);
    }

    public void criar(String nome, String email) {
        ProfessorModel professor = new ProfessorModel(nome, email);
        banco.preencherListaProfessor(professor);
    }

    public LinkedList listar() {
        return banco.getListaProfessor();
    }
}
