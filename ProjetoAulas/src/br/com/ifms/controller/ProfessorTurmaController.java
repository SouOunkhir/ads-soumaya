
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.ProfessorModel;
import br.com.ifms.model.*;
import java.util.LinkedList;

public class ProfessorTurmaController {
    private ProfessorTurmaModel buscado;
    
    Banco banco = new Banco();
    LinkedList<ProfessorTurmaModel> lista = new LinkedList<ProfessorTurmaModel>();

    public void salvar(ProfessorModel professor, TurmaModel turma) {
       ProfessorTurmaModel professorTurma = new ProfessorTurmaModel(professor, turma);
        lista.add(professorTurma);
    }

    public void criar(ProfessorModel professor, TurmaModel turma) {
       ProfessorTurmaModel professorTurma = new ProfessorTurmaModel(professor, turma);
        lista.add(professorTurma);
    }

    public void alterar(ProfessorTurmaModel velha, ProfessorTurmaModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(ProfessorTurmaModel professorTurma) {
        buscar(professorTurma);
        lista.remove(professorTurma);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public ProfessorTurmaModel buscar(ProfessorTurmaModel professorTurma) {
        for (int i = 0; i < lista.size(); i++) {
            if (professorTurma.equals(lista.get(i))) {
                buscado = professorTurma;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
