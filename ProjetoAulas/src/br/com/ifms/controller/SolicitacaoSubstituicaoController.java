
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class SolicitacaoSubstituicaoController {
    private SolicitacaoSubstituicaoModel buscado;
    
    Banco banco = new Banco();
    LinkedList<SolicitacaoSubstituicaoModel> lista = new LinkedList<SolicitacaoSubstituicaoModel>();

    public void salvar(List<ItemSubstituicaoModel> listaDeItens, String coordenacaoLotacao, Boolean coordenacaoAutorizado, Date dataCiencia, String nome, int matricula, String cargo, String setorLocacao, int telefone, String email) {
       SolicitacaoSubstituicaoModel solicitacaoSubstituicao = new SolicitacaoSubstituicaoModel(listaDeItens, coordenacaoLotacao, coordenacaoAutorizado, dataCiencia, nome, matricula, cargo, setorLocacao, telefone, email);
        lista.add(solicitacaoSubstituicao);
    }

    public void criar(List<ItemSubstituicaoModel> listaDeItens, String coordenacaoLotacao, Boolean coordenacaoAutorizado, Date dataCiencia, String nome, int matricula, String cargo, String setorLocacao, int telefone, String email) {
       SolicitacaoSubstituicaoModel solicitacaoSubstituicao = new SolicitacaoSubstituicaoModel(listaDeItens, coordenacaoLotacao, coordenacaoAutorizado, dataCiencia, nome, matricula, cargo, setorLocacao, telefone, email);
        lista.add(solicitacaoSubstituicao);
    }

    public void alterar(SolicitacaoSubstituicaoModel velha, SolicitacaoSubstituicaoModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(SolicitacaoSubstituicaoModel solicitacaoSubstituicao) {
        buscar(solicitacaoSubstituicao);
        lista.remove(solicitacaoSubstituicao);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public SolicitacaoSubstituicaoModel buscar(SolicitacaoSubstituicaoModel solicitacaoSubstituicao) {
        for (int i = 0; i < lista.size(); i++) {
            if (solicitacaoSubstituicao.equals(lista.get(i))) {
                buscado = solicitacaoSubstituicao;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
