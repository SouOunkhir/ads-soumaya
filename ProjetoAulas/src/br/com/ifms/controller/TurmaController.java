
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.util.LinkedList;


public class TurmaController {

    static Banco banco = new Banco();

    public void salvar(String curso, int num, String email, String lider) {
        TurmaModel turma = new TurmaModel(curso, num, email, lider);
        banco.preencherListaTurma(turma);
    }

    public void criar(String nome, int num, String email, String lider) {
        TurmaModel turma = new TurmaModel(nome, num, email, lider);
        banco.preencherListaTurma(turma);
    }

    public LinkedList listar() {
        return banco.getListaTurma();
    }
}
