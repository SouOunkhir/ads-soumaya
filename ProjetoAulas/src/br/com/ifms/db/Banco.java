package br.com.ifms.db;

import br.com.ifms.model.*;
import java.util.LinkedList;

public class Banco {

    private LinkedList<ProfessorModel> listaProfessor;
    private LinkedList<TurmaModel> listaTurma;    

    public LinkedList<ProfessorModel> getListaProfessor() {
        return listaProfessor;
    }

    public void setListaProfessor(LinkedList<ProfessorModel> listaProfessor) {
        this.listaProfessor = listaProfessor;
    }

    public LinkedList<TurmaModel> getListaTurma() {
        return listaTurma;
    }

    public void setListaTurma(LinkedList<TurmaModel> listaTurma) {
        this.listaTurma = listaTurma;
    }
    
    
    public Banco() {
        listaProfessor = new LinkedList<>();
        listaTurma = new LinkedList<>();
    }
    public void preencherListaProfessor(ProfessorModel professor){
        listaProfessor.add(professor);
        listaProfessor.size();
    }
     public void preencherListaTurma(TurmaModel turma){
        listaTurma.add(turma);
        listaTurma.size();
    } 
}
