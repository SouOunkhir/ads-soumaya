
package br.com.ifms.model;


public class DisciplinaModel {
    private String nome;

    public DisciplinaModel(String nome){
        this.nome = nome;
    }

    public String getNome(){
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    } 
}
