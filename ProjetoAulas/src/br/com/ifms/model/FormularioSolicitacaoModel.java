
package br.com.ifms.model;


public class FormularioSolicitacaoModel {
    private String nome;
    private int matricula;
    private String cargo;
    private String setorLocacao;
    private int telefone;
    private String email;

    public FormularioSolicitacaoModel(String nome, int matricula, String cargo, String setorLocacao, int telefone, String email) {
        this.nome = nome;
        this.matricula = matricula;
        this.cargo = cargo;
        this.setorLocacao = setorLocacao;
        this.telefone = telefone;
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getSetorLocacao() {
        return setorLocacao;
    }

    public void setSetorLocacao(String setorLocacao) {
        this.setorLocacao = setorLocacao;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
