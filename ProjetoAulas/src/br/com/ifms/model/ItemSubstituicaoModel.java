
package br.com.ifms.model;


public class ItemSubstituicaoModel {
    private String unidadeCurricular;
    private String curso;
    private String dataSubstituicao;
    private String objetivoAula;
    private String conteudoMinistrado;
    private String atividadesDesenvolvidas;
    private String nomeSubstituto;

    public ItemSubstituicaoModel(String unidadeCurricular, String curso, String dataSubstituicao, String objetivoAula, String conteudoMinistrado, String atividadesDesenvolvidas, String nomeSubstituto) {
        this.unidadeCurricular = unidadeCurricular;
        this.curso = curso;
        this.dataSubstituicao = dataSubstituicao;
        this.objetivoAula = objetivoAula;
        this.conteudoMinistrado = conteudoMinistrado;
        this.atividadesDesenvolvidas = atividadesDesenvolvidas;
        this.nomeSubstituto = nomeSubstituto;
    }

    public String getUnidadeCurricular() {
        return unidadeCurricular;
    }

    public void setUnidadeCurricular(String unidadeCurricular) {
        this.unidadeCurricular = unidadeCurricular;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getDataSubstituicao() {
        return dataSubstituicao;
    }

    public void setDataSubstituicao(String dataSubstituicao) {
        this.dataSubstituicao = dataSubstituicao;
    }

    public String getObjetivoAula() {
        return objetivoAula;
    }

    public void setObjetivoAula(String objetivoAula) {
        this.objetivoAula = objetivoAula;
    }

    public String getConteudoMinistrado() {
        return conteudoMinistrado;
    }

    public void setConteudoMinistrado(String conteudoMinistrado) {
        this.conteudoMinistrado = conteudoMinistrado;
    }

    public String getAtividadesDesenvolvidas() {
        return atividadesDesenvolvidas;
    }

    public void setAtividadesDesenvolvidas(String atividadesDesenvolvidas) {
        this.atividadesDesenvolvidas = atividadesDesenvolvidas;
    }

    public String getNomeSubstituto() {
        return nomeSubstituto;
    }

    public void setNomeSubstituto(String nomeSubstituto) {
        this.nomeSubstituto = nomeSubstituto;
    }
    
    
}
