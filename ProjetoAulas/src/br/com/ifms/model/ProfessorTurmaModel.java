
package br.com.ifms.model;

public class ProfessorTurmaModel {
    private ProfessorModel professor;
    private TurmaModel turma;

    public ProfessorTurmaModel(ProfessorModel professor, TurmaModel turma) {
        this.professor = professor;
        this.turma = turma;
    }

    public ProfessorModel getProfessor() {
        return professor;
    }

    public void setProfessor(ProfessorModel professor) {
        this.professor = professor;
    }

    public TurmaModel getTurma() {
        return turma;
    }

    public void setTurma(TurmaModel turma) {
        this.turma = turma;
    }
    
    
}
