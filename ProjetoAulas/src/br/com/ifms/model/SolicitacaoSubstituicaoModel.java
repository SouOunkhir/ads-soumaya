
package br.com.ifms.model;

import java.util.Date;
import java.util.List;


public class SolicitacaoSubstituicaoModel  extends FormularioSolicitacaoModel{
    private List <ItemSubstituicaoModel> listaDeItens;
    private String coordenacaoLotacao;
    private Boolean coordenacaoAutorizado;
    private Date dataCiencia;

    public SolicitacaoSubstituicaoModel(List<ItemSubstituicaoModel> listaDeItens, String coordenacaoLotacao, Boolean coordenacaoAutorizado, Date dataCiencia, String nome, int matricula, String cargo, String setorLocacao, int telefone, String email) {
        super(nome, matricula, cargo, setorLocacao, telefone, email);
        this.listaDeItens = listaDeItens;
        this.coordenacaoLotacao = coordenacaoLotacao;
        this.coordenacaoAutorizado = coordenacaoAutorizado;
        this.dataCiencia = dataCiencia;
    }

    public List<ItemSubstituicaoModel> getListaDeItens() {
        return listaDeItens;
    }

    public void setListaDeItens(List<ItemSubstituicaoModel> listaDeItens) {
        this.listaDeItens = listaDeItens;
    }

    public String getCoordenacaoLotacao() {
        return coordenacaoLotacao;
    }

    public void setCoordenacaoLotacao(String coordenacaoLotacao) {
        this.coordenacaoLotacao = coordenacaoLotacao;
    }

    public Boolean getCoordenacaoAutorizado() {
        return coordenacaoAutorizado;
    }

    public void setCoordenacaoAutorizado(Boolean coordenacaoAutorizado) {
        this.coordenacaoAutorizado = coordenacaoAutorizado;
    }

    public Date getDataCiencia() {
        return dataCiencia;
    }

    public void setDataCiencia(Date dataCiencia) {
        this.dataCiencia = dataCiencia;
    }
}
