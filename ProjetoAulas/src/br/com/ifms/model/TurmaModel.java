
package br.com.ifms.model;


public class TurmaModel {
    private String curso;
    private int numero;
    private String email;
    private String lider ;

    public TurmaModel(String curso, int numero, String email, String lider) {
        this.curso = curso;
        this.numero = numero;
        this.email = email;
        this.lider = lider;
    }  

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLider() {
        return lider;
    }

    public void setLider(String lider) {
        this.lider = lider;
    }
    
}
