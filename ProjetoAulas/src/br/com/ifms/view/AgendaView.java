package br.com.ifms.view;

import br.com.ifms.controller.ProfessorController;
import br.com.ifms.controller.TurmaController;
import br.com.ifms.model.ProfessorModel;
import br.com.ifms.model.TurmaModel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.LinkedList;

public class AgendaView {

    private JFrame janela;
    private JPanel painelProfessor;
    private JPanel painelTurma;
    private JPanel painelOeste;

    private JButton botaoCriarProf;
    private JButton botaoAlterarProf;
    private JButton botaoExcluirProf;
    private JButton botaoConsultarProf;

    private JButton botaoCriarTurma;
    private JButton botaoAlterarTurma;
    private JButton botaoExcluirTurma;
    private JButton botaoConsultarTurma;

    public void construir() {
        janela = new JFrame("Sistema Substituição"); //Nome da janela LP2
        janela.setBounds(200, 30, 700, 400); // setBounds(x,y,tamx,tamy)
        janela.getContentPane().setLayout(new BorderLayout());

        painelProfessor = new JPanel(new GridLayout(4, 1));
        painelTurma = new JPanel(new GridLayout(4, 1));
        painelOeste = new JPanel(new GridLayout(2, 1));

        botaoCriarProf = new JButton("Criar Professor");
        botaoAlterarProf = new JButton("Alterar professor");
        botaoExcluirProf = new JButton("Excluir Professor");
        botaoConsultarProf = new JButton("Consultar Professor");

        botaoCriarTurma = new JButton("Criar Turma");
        botaoAlterarTurma = new JButton("Alterar Turma");
        botaoExcluirTurma = new JButton("Excluir Turma");
        botaoConsultarTurma = new JButton("Consultar Turma");

        painelProfessor.add(botaoCriarProf);
        painelProfessor.add(botaoAlterarProf);
        painelProfessor.add(botaoExcluirProf);
        painelProfessor.add(botaoConsultarProf);

        painelTurma.add(botaoCriarTurma);
        painelTurma.add(botaoAlterarTurma);
        painelTurma.add(botaoExcluirTurma);
        painelTurma.add(botaoConsultarTurma);

        painelOeste.add(painelProfessor);
        painelOeste.add(painelTurma);

        janela.getContentPane().add(painelOeste, BorderLayout.WEST);

        // janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        janela.setVisible(true);
        janela.setResizable(false);
        acoesBotoes();
    }

    public void acoesBotoes() {
        botaoCriarProf.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                CriarProfessorView j = new CriarProfessorView();
                j.construir();
            }
        });
        botaoConsultarProf.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                
                ListarProfessorView telaListarProf = new ListarProfessorView();
                telaListarProf.construir(/*lista*/);
            }
        });

        botaoCriarTurma.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                CriarTurmaView j = new CriarTurmaView();
                j.construir();
                //janela.dispose();
                janela.requestFocus();
            }
        });
        botaoConsultarTurma.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {

                ListarTurmaView telaListarTurma = new ListarTurmaView();
                telaListarTurma.construir();
                janela.requestFocus();
            }
        });
    }
}
