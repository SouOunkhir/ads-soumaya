package br.com.ifms.view;

import br.com.ifms.controller.TurmaController;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CriarTurmaView {

    private JTextField textoCurso;
    private JTextField textoEmail;
    private JTextField textoNum;
    private JTextField textoLider;
    private JLabel labelEmail;
    private JLabel labelLider;
    private JLabel labelNum;
    private JLabel labelCurso;
    private JFrame janela;
    private JPanel linhaUm;
    private JPanel linhaDois;
    private JPanel linhaTres;
    private JPanel linhaQuatro;
    private JPanel linhaCinco;
    private JButton enviar;

    private JPanel painelCentro;

    public void construir() {
        janela = new JFrame("Cadastrar Turma");
        janela.setBounds(350, 62, 700, 220);
        janela.getContentPane().setLayout(new FlowLayout());

        painelCentro = new JPanel(new GridLayout(5, 1));

        textoCurso = new JTextField();
        textoNum = new JTextField();
        textoEmail = new JTextField();
        textoLider = new JTextField();
        labelCurso = new JLabel("Insira o nome do curso: ");
        labelNum = new JLabel("Insira o número da turma: ");
        labelEmail = new JLabel("Insira o email da turma: ");
        labelLider = new JLabel("Insira o nome do líder da turma: ");
        linhaUm = new JPanel(new GridLayout(1, 2));
        linhaDois = new JPanel(new GridLayout(1, 2));
        linhaTres = new JPanel(new GridLayout(1, 2));
        linhaQuatro = new JPanel(new GridLayout(1, 2));
        linhaCinco = new JPanel();
        enviar = new JButton("Enviar");

        textoCurso.setPreferredSize(new Dimension(300, 30));
        textoEmail.setPreferredSize(new Dimension(300, 30));

        linhaUm.add(labelCurso);
        linhaUm.add(textoCurso);
        linhaDois.add(labelEmail);
        linhaDois.add(textoEmail);
        linhaTres.add(labelNum);
        linhaTres.add(textoNum);
        linhaQuatro.add(labelLider);
        linhaQuatro.add(textoLider);
        linhaCinco.add(enviar);

        painelCentro.add(linhaUm);
        painelCentro.add(linhaDois);
        painelCentro.add(linhaTres);
        painelCentro.add(linhaQuatro);
        painelCentro.add(linhaCinco);
        janela.getContentPane().add(painelCentro);
        janela.setResizable(false);
        acoesBotoes();

        janela.setVisible(true);
    }

    public void acoesBotoes() {
        enviar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                String nome = textoCurso.getText();
                String email = textoEmail.getText();
                String lider = textoLider.getText();
                int num = Integer.parseInt(textoNum.getText());
                
                TurmaController p = new TurmaController();
                p.criar(nome, num, email, lider);
                janela.requestFocus();

                janela.dispose();

            }
        });
    }
}
