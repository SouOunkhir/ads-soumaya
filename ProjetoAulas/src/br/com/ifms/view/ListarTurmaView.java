package br.com.ifms.view;

import br.com.ifms.controller.TurmaController;
import br.com.ifms.model.ProfessorModel;
import br.com.ifms.model.TurmaModel;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ListarTurmaView {

    private JLabel labelCurso;
    private JLabel labelNum;
    private JLabel labelEmail;
    private JLabel labelLider;
    private JFrame janela;
    private JPanel linhaUm;
    private JPanel linhaDois;
    private JPanel linhaTres;
    private JPanel linhaQuatro;
    private JPanel painelCentro;

    public void construir() {

        TurmaController p = new TurmaController();
        LinkedList<TurmaModel> lista = p.listar();
        janela = new JFrame("Turmas");
        janela.setBounds(350, 62, 700, 220);
        janela.getContentPane().setLayout(new GridLayout(lista.size(), 1, 0, 5));
        
        
        
        for (TurmaModel i : lista) {
            painelCentro = new JPanel(new GridLayout(4, 1));

            labelCurso = new JLabel("Curso: " + i.getCurso());
            labelEmail = new JLabel("Email da turma: " + i.getEmail());
            labelNum = new JLabel("Número da turma: " + i.getNumero());
            labelLider = new JLabel("Líder da turma: " + i.getLider());

            linhaUm = new JPanel(new GridLayout(1, 1));
            linhaDois = new JPanel(new GridLayout(1, 1));
            linhaTres = new JPanel(new GridLayout(1, 1));
            linhaQuatro = new JPanel(new GridLayout(1, 1));

            linhaUm.add(labelCurso);
            linhaDois.add(labelEmail);
            linhaTres.add(labelNum);
            linhaQuatro.add(labelLider);

            painelCentro.add(linhaUm);
            painelCentro.add(linhaDois);
            painelCentro.add(linhaTres);
            painelCentro.add(linhaQuatro);

            janela.getContentPane().add(painelCentro);
    }
        janela.setResizable(false);
        janela.setVisible(true);
    }

}
