
package br.com.ifms.view;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import br.com.ifms.controller.ContatoController;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;


public class InserirContatoView {
    
    private JFrame janela;
    private DefaultTableModel modelo = new DefaultTableModel();
    private JPanel painelFundo;
    private JButton btSalvar;
    private JButton btLimpar;
    private JLabel lbId;
    private JLabel lbNome;
    private JLabel lbTelefone;
    private JLabel lbEmail;
    private JTextField txId;
    private JTextField txNome;
    private JTextField txTelefone;
    private JTextField txEmail;
    private ContatoController cc;
    
    public void criaJanela(){
        
        janela = new JFrame("CONTATOS");
        janela.setBounds(200, 30, 350, 150); 
        janela.getContentPane();
        
        btSalvar = new JButton("Salvar");
        btLimpar = new JButton("Limpar");
        
        painelFundo = new JPanel();
        painelFundo.setLayout(new GridLayout(5, 2));
        
        lbId = new JLabel("Id");
        lbNome = new JLabel("Nome");
        lbTelefone = new JLabel("Telefone");
        lbEmail = new JLabel("Email");
        
        txId = new JTextField("Digite...");
        txNome = new JTextField("Digite...");
        txTelefone = new JTextField("Digite...");
        txEmail = new JTextField("Digite...");
        
        painelFundo.add(lbId);
        painelFundo.add(txId);
        
        painelFundo.add(lbNome);
        painelFundo.add(txNome);
        
        painelFundo.add(lbTelefone);
        painelFundo.add(txTelefone);
        
        painelFundo.add(lbEmail);
        painelFundo.add(txEmail);
        
        painelFundo.add(btSalvar);
        painelFundo.add(btLimpar);
        
        janela.add(painelFundo);
        
        janela.setVisible(true);
        janela.setResizable(true);
    }
    
    public void InserirContato(DefaultTableModel md){}
    
    public static void main(String[] args) {
        
        InserirContatoView ic = new InserirContatoView();
        
        ic.criaJanela();
    }
}
